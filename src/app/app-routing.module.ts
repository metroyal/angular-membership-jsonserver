import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DataMemberComponent } from './data-member/data-member.component';
import { RegistrationComponent } from './registration/registration.component';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    data: {
      title: 'Membership'
    }
  },
  {
    path : 'membership',
    component: DataMemberComponent,
    data: {
      title: 'Data Member'
    }
  },
  {
    path : 'registration',
    component: RegistrationComponent,
    data: {
      title: 'Register New Member'
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
