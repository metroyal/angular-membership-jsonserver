export class UserModel {
    constructor(initial?: Partial<UserModel>) {
        Object.assign(this, initial);
    }
    public id: number;
    public email: string;
    public fname: string;
    public lname: string;
    public password: string;
    public address: string;
    public phone: any;
}