import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Configuration } from '../config/config';
import { UserModel } from '../models/user.model';

@Injectable({
    providedIn: 'root'
})
export class MemberService {
    constructor(private http: HttpClient) { }

    fetchMember(): Observable<UserModel> {
        return this.http.get(Configuration.apiURL + '/posts')
        .pipe(
            map(response => response as UserModel)
        );
    }

    getMemberEdit(data: number): Observable<UserModel> {
        return this.http.get(Configuration.apiURL + '/posts/' + data)
        .pipe(
            map(response => response as UserModel)
        );
    }

    addMember(data: UserModel): Observable<UserModel> {
        return this.http.post(Configuration.apiURL + '/posts', data)
        .pipe(
            map(response => response as UserModel)
        );
    }

    updateMember(data: UserModel, id: number) {
        return this.http.put(Configuration.apiURL + '/posts/' + id, data)
        .pipe(
            map(response => response as UserModel)
            // catchError(this.handleError)
        );
    }

    removeMember(data: number): Observable<UserModel> {
        return this.http.delete<UserModel>(Configuration.apiURL + '/posts/' + data)
        .pipe(
            map(response => response as UserModel)
        );
    }
}
