import { Component, OnInit, Input } from '@angular/core';
import { UserModel } from '../core/models/user.model';
import { MemberService } from '../core/services/member.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-data-member',
  templateUrl: './data-member.component.html',
  styleUrls: ['./data-member.component.scss']
})
export class DataMemberComponent implements OnInit {
  idObject: number;
  member$;
  editList;
  profileForm: FormGroup;

  private formData() {
    this.profileForm = this.fb.group({
      email: ['', (Validators.pattern('^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$'))],
      fname: ['', Validators.required],
      lname: ['', Validators.required],
      password: ['', (Validators.pattern('^(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$'))],
      address: ['', Validators.required],
      phone: ['', Validators.required],
    });
  }

  @Input() userList: UserModel[] = [];
  constructor(
    private fb: FormBuilder,
    private memberService: MemberService
  ) {
    this.formData();
  }

  ngOnInit() {
    this.fetchMember();
  }

  getMemberEdit(id) {
    this.memberService.getMemberEdit(id).subscribe(result => {
      this.editList = result;
      this.profileForm.patchValue({
        email: result.email,
        fname: result.fname,
        lname: result.lname,
        password: result.password,
        address: result.address,
        phone: result.phone
      });
      this.profileForm.updateValueAndValidity();
    });
  }

  fetchMember() {
    this.memberService.fetchMember().subscribe(result => {
      this.member$ = result;
    });
  }

  idDelete(i) {
    this.idObject = i;
  }

  deleteItem() {
    const index = this.userList.indexOf[this.idObject];
    this.userList.splice(index, 1);
  }

  addCode() {
    const phone = this.fb.group(this.profileForm.value).value.phone;
    this.profileForm.value.phone = '+62' + phone;
  }

  updateUser(id) {
    this.addCode();
    const form =  this.profileForm;
    const data: UserModel = new UserModel();
    data.email = form.value.email;
    data.fname = form.value.fname;
    data.lname = form.value.lname;
    data.password = form.value.password;
    data.address = form.value.address;
    data.phone = form.value.phone;
    this.memberService.updateMember(data, id).subscribe(result => {
      console.log('update :', result);
      this.fetchMember();
      // jquery.$('#myModal').modal('hide');
    });
  }

  closeOneModal() {
    // get modals
    const modals = document.getElementsByClassName('modal');

    // on every modal change state like in hidden modal
    for(let i =0; i<modals.length; i++) {
      modals[i].classList.remove('show');
      modals[i].setAttribute('aria-hidden', 'true');
      modals[i].setAttribute('style', 'display: none');
    }

    // get modal backdrops
    const modalsBackdrops = document.getElementsByClassName('modal-backdrop');

    // remove every modal backdrop
    for(let i= 0; i<modalsBackdrops.length; i++) {
      document.body.removeChild(modalsBackdrops[i]);
    }
  }

  deleteUser() {
      const index = this.idObject;
      this.memberService.removeMember(index).subscribe(result => {
        this.fetchMember();
    });
  }
}
