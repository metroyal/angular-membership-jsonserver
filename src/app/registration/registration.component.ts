import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormGroupDirective, FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { UserModel } from '../core/models/user.model';
import { MemberService } from '../core/services/member.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  profileForm: FormGroup;

  // @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  @Output() public onCreate: EventEmitter<UserModel> = new EventEmitter();

  private formData() {
    this.profileForm = this.fb.group({
      email: ['', (Validators.pattern('^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$'))],
      fname: ['', Validators.required],
      lname: ['', Validators.required],
      password: ['', (Validators.pattern('^(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$'))],
      repassword: ['', Validators.required],
      address: ['', Validators.required],
      phone: ['', Validators.required],
    }, {
      validator: this.checkPasswords
    });
  }

  constructor(
      private fb: FormBuilder,
      private addMember: MemberService
    ) {
    this.formData();
  }

  ngOnInit() {
  }

  checkPasswords(group: FormGroup) {
    const pass = group.get('password').value;
    const confirmPass = group.get('repassword').value;

    return pass === confirmPass ? null : { notSame: true };
  }

  addCode() {
    const phone = this.fb.group(this.profileForm.value).value.phone;
    this.profileForm.value.phone = '+62' + phone;
    console.log('this.profileForm.value.phone :', this.profileForm.value.phone);
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    // console.warn(this.profileForm.value);
    // const user = new UserModel(this.profileForm.value);
    // this.onCreate.emit(user);

    this.profileForm.reset();
    this.profileForm.controls.controlName.clearValidators();
    this.profileForm.controls.controlName.updateValueAndValidity();
  }

  onSubmitJSON() {
    this.addCode();
    const form =  this.profileForm;
    const data: UserModel = new UserModel();
    data.email = form.value.email;
    data.fname = form.value.fname;
    data.lname = form.value.lname;
    data.password = form.value.password;
    data.address = form.value.address;
    data.phone = form.value.phone;
    this.addMember.addMember(data).subscribe(response => {
      // this.formGroupDirective.resetForm();
      this.profileForm.reset();
      this.profileForm.updateValueAndValidity();
      Swal.fire({
        icon: 'success',
        title: 'Saved',
        text: 'You are now registered as member!'
      });
    });
  }
}
