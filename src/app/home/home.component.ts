import { Component, OnInit } from '@angular/core';
import { UserModel } from '../core/models/user.model';
import { MemberService } from '../core/services/member.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  membership: boolean = true;
  registration: boolean = false;
  newUser: UserModel;

  listUser: UserModel[] = [];
  constructor() { }

  ngOnInit() {
  }

  memberTab() {
    this.membership = true;
    this.registration = false;
  }

  regisTab() {
    this.membership = false;
    this.registration = true;
  }

  fetchData(UserModel) {
    console.log('User', UserModel);
    this.listUser.push(UserModel);
    console.log('this.listUser', this.listUser);
  }
}
